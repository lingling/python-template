from src.controller import Controller


def main():
    Controller().read_arguments()


if __name__ == "__main__":
    main()
